-- update ASSISTANCE_BRANCH set ADDRESS_COUNTRY_ID = <default_value> where ADDRESS_COUNTRY_ID is null ;
alter table ASSISTANCE_BRANCH alter column ADDRESS_COUNTRY_ID set not null ;
-- update ASSISTANCE_BRANCH set ADDRESS_REGION_ID = <default_value> where ADDRESS_REGION_ID is null ;
alter table ASSISTANCE_BRANCH alter column ADDRESS_REGION_ID set not null ;
